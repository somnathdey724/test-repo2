package com.javacodegeeks.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.util.jndi.JndiContext;

public class CamelMockExample {
	public static void main(String[] args) throws Exception {
		JndiContext jndiContext = new JndiContext();
		jndiContext.bind("greetingBean", new Greeting());
		CamelContext camelContext = new DefaultCamelContext(jndiContext);
		try {
			camelContext.addRoutes(new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:in").to("bean:greetingBean").to("mock:out");
				}
			});
			camelContext.start();
			Thread.sleep(1000);
			ProducerTemplate template = camelContext.createProducerTemplate();
			template.sendBody("direct:in", "This is mock example");
			MockEndpoint resultEndpoint = camelContext.getEndpoint("mock:out", MockEndpoint.class);
			resultEndpoint.expectedMessageCount(1);
			System.out.println("Message received: " + resultEndpoint.getExchanges().get(0).getIn().getBody());
			resultEndpoint.expectedBodiesReceived("Hello This is mock example");
		} finally {
			camelContext.stop();
		}
	}
}

package com.javacodegeeks.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.seda.SedaComponent;
import org.apache.camel.impl.DefaultCamelContext;

public class CamelAddingComponentExample {
	public static void main(String[] args) throws Exception {
		CamelContext camelContext = new DefaultCamelContext();
		try {
			camelContext.addComponent("activemq", new SedaComponent());
			camelContext.addRoutes(new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("direct:in").to("activemq:someQueue");
					from("activemq:someQueue").to("stream:out");
				}
			});
			camelContext.start();
			ProducerTemplate template = camelContext.createProducerTemplate();
			template.sendBody("direct:in", "Adding camel component");
		} finally {
			camelContext.stop();
		}
	}
}

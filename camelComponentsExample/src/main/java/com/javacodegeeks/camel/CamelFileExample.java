package com.javacodegeeks.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.util.jndi.JndiContext;

public class CamelFileExample {
	public static final void main(String[] args) throws Exception {
		JndiContext jndiContext = new JndiContext();
		jndiContext.bind("greetingBean", new Greeting());
		CamelContext camelContext = new DefaultCamelContext(jndiContext);
		try {
			camelContext.addRoutes(new RouteBuilder() {
				public void configure() {
					from("direct:fileContent").to(
							"file:target/?fileName=out.txt").to("stream:out");
					from("file://target/?fileName=out.txt&move=processed")
							.process(new Processor() {
								public void process(Exchange exchange)
										throws Exception {
									Message in = exchange.getIn();
									in.setBody(in.getBody(String.class)
											+ "(polling)");
								}
							}).to("bean:greetingBean").to("stream:out");
				}
			});
			ProducerTemplate template = camelContext.createProducerTemplate();
			camelContext.start();
			template.sendBody("direct:fileContent", "This is file example");
			Thread.sleep(3000);
		} finally {
			camelContext.stop();
		}
	}
}

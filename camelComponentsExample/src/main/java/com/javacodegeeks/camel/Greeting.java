package com.javacodegeeks.camel;

import java.util.ArrayList;
import java.util.List;

public class Greeting {
	private List<String> messages = new ArrayList<String>();

	public String hello(String msg) {
		String helloMsg = "Hello " + msg;
		messages.add(helloMsg);
		return helloMsg;
	}

	public String toString() {
		return messages.toString();
	}
}

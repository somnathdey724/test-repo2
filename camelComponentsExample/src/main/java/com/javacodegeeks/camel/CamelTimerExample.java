package com.javacodegeeks.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.util.jndi.JndiContext;

public class CamelTimerExample {
	public static void main(String[] args) throws Exception {
		JndiContext jndiContext = new JndiContext();
		jndiContext.bind("greetingBean", new Greeting());
		CamelContext camelContext = new DefaultCamelContext(jndiContext);
		try {
			camelContext.addRoutes(new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					from("timer://timerExample?period=2000")
							.setBody()
							.simple("This is timer example ${header.firedTime}")
							.to("bean:greetingBean")
							.to("stream:out");
					;
				}
			});
			camelContext.start();
			Thread.sleep(10000);
		} finally {
			camelContext.stop();
		}
	}
}
